package com.example.bakery.labrecycleview

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.activity_detail.*
import kotlinx.android.synthetic.main.list_item.view.*

class DetailActivity : AppCompatActivity() {
    var listData = arrayListOf<ItemBean>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)

        /* val username = intent.getStringExtra("name")
         val password = intent.getStringExtra("password")*/


        addData()

        rvRecycle.layoutManager = LinearLayoutManager(this)
        rvRecycle.adapter = Adapter()

    }

    private fun addData() {
        for (i in 1..100) {
            listData.add(ItemBean("Bakery", "Ton", "Index: $i"))
        }
        
    }
    inner class Adapter: RecyclerView.Adapter<CustomHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomHolder {
            val view = LayoutInflater.from(parent.context).inflate(R.layout.list_item, parent, false)
            return CustomHolder(view)
        }

        override fun getItemCount(): Int {
            return listData.size
        }

        override fun onBindViewHolder(holder: CustomHolder, position: Int) {
            holder.tvTitle.text = listData[position].title
            holder.tvSupTitle.text = listData[position].subtitle

        }
    }

    inner class CustomHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        var tvTitle = itemView.tvTitle
        var tvSupTitle = itemView.tvSubTitle
        var ivmage = itemView.ivImage
    }
}







