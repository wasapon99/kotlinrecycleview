package com.example.bakery.labrecycleview

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btnLogin.setOnClickListener {
            val username:String? = etUsername.text.toString()
            val password:String? = etPassword.text.toString()

            val intent = Intent(this,DetailActivity::class.java)
           /* intent.putExtra("name",username)
            intent.putExtra("password",password)*/

            val acc = Account(username, password)
            intent.putExtra("Acc",acc)
            startActivity(intent)
            Toast.makeText(this, "Login: $username $password" , Toast.LENGTH_SHORT).show()
        }
        btnRegister.setOnClickListener {
            Toast.makeText(this, "Register:", Toast.LENGTH_SHORT).show()

        }
    }
}
